package com.renge;

public interface HelloRrpc {
    /**
     * 通用接口：server和client都需要依赖
     * @param msg  发送的消息
     * @return
     */
    String sayHi(String msg);
}
