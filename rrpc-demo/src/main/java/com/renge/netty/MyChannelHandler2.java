package com.renge.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.nio.charset.StandardCharsets;

/**
 * _*_ coding : utf-8 _*_
 *
 * @Time : 2023/8/5 21:26
 * @Author : 计201任思伟
 * @File : MyChannelHandler
 * @Project : renge_rpc
 */
//处理器   客户端处理服务端的消息
public class MyChannelHandler2 extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf byteBuf= (ByteBuf) msg;
        System.out.println("客户端已经收到了消息===>"+byteBuf.toString(StandardCharsets.UTF_8));

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        //出现异常执行的操作
        cause.printStackTrace();
        ctx.close();
    }
}
