package com.renge.netty;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * _*_ coding : utf-8 _*_
 *
 * @Time : 2023/8/5 21:26
 * @Author : 计201任思伟
 * @File : MyChannelHandler
 * @Project : renge_rpc
 */
//处理器   客户端发送给服务端的请求   是in操作
public class MyChannelHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        //处理收到的消息并返回给客户端
        ByteBuf byteBuf= (ByteBuf) msg;
        System.out.println("服务端已经收到了消息===>"+byteBuf.toString(StandardCharsets.UTF_8));

        //可以通过ctx获取channel  必须发送byteBuf  服务端到客户端的handler需要序列化
//        ctx.channel().writeAndFlush("hello client");
        ctx.channel().writeAndFlush(Unpooled.copiedBuffer("hello client".getBytes(StandardCharsets.UTF_8)));
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        //出现异常执行的操作
        cause.printStackTrace();
        ctx.close();
    }
}
