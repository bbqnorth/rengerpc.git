package com.renge.netty;


import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;

/**
 * _*_ coding : utf-8 _*_
 *
 * @Time : 2023/8/7 18:01
 * @Author : 计201任思伟
 * @File : MyWatcher
 * @Project : renge_rpc
 */
public class MyWatcher implements Watcher {
    /**
     * watcher只写自己关心的事件
     * @param event
     */
    @Override
    public void process(WatchedEvent event) {
        //判断时间类型 ，连接类型的事件还是节点类型的事件
        if (event.getType() == Watcher.Event.EventType.None){
            //None连接类型的事件
            if (event.getState() == Event.KeeperState.SyncConnected){
                System.out.println("zookeeper 连接成功");
            }else if (event.getState() == Event.KeeperState.Disconnected){
                System.out.println("zookeeper 断开连接");
            }else if (event.getState() == Event.KeeperState.AuthFailed){
                System.out.println("zookeeper 认证失败");
            }else if (event.getState() == Event.KeeperState.Expired){
                System.out.println("zookeeper session超时");
            }
        } else if (event.getType() == Event.EventType.NodeCreated) {
            System.out.println(event.getPath()+"被创建了");
        }else if (event.getType() == Event.EventType.NodeDeleted) {
            System.out.println(event.getPath()+"被删除了");
        }else if (event.getType() == Event.EventType.NodeChildrenChanged) {
            System.out.println(event.getPath()+"节点的子节点改变了");
        }else if (event.getType() == Event.EventType.NodeDataChanged) {
            System.out.println(event.getPath()+"节点的数据改变了");
        }
    }
}
