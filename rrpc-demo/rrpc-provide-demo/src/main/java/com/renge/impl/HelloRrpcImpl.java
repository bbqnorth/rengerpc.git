package com.renge.impl;

import com.renge.HelloRrpc;

/**
 * _*_ coding : utf-8 _*_
 *
 * @Time : 2023/8/8 19:17
 * @Author : 计201任思伟
 * @File : HelloRrpcImpl
 * @Project : renge_rpc
 */
public class HelloRrpcImpl implements HelloRrpc {
    @Override
    public String sayHi(String msg) {
        return "hi consumer :"+msg;
    }
}
